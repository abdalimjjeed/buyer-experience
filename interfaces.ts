export interface Breadcrumb {
  title: string;
  href?: string;
  data_ga_name?: string;
  data_ga_location?: string;
}
