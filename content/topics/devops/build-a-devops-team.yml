---
  title: Create the ideal DevOps team structure
  description: The DevOps platform is in place but is the team structure ready for prime time? Here’s what you need to consider when building a DevOps team.
  partenttopic: devops
  twitter_image: /nuxt-images/open-graph/gitlab-blog-cover.png
  date_published: 2023-03-23
  date_modified: 2023-03-30
  topics_header:
      data:
        title:  Create the ideal DevOps team structure
        block:
          - metadata:
              id_tag: what-is-devops
            text: |
              Finding the right balance in your DevOps team is not a one-size-fits-all proposition.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: DevOps
      href: /topics/devops/
      data_ga_name: devops
      data_ga_location: breadcrumb
    - title: Building a DevOps team
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: Types of silos
          href: "#types-of-silos"
          data_ga_name: Types of silos
          data_ga_location: side-navigation
        - text: Dev and ops are completely separate
          href: "#dev-and-ops-are-completely-separate"
          data_ga_name: Dev and ops are completely separate
          data_ga_location: side-navigation
        - text: DevOps middleman
          href: "#dev-ops-middleman"
          data_ga_name: DevOps middleman
          data_ga_location: side-navigation
        - text: Ops stands alone
          href: "#ops-stands-alone"
          data_ga_name: Ops stands alone
          data_ga_location: side-navigation
        - text: What can DevOps team leadership do?
          href: "#what-can-dev-ops-team-leadership-do"
          data_ga_name: What can DevOps team leadership do?
          data_ga_location: side-navigation
        - text: DevOps roles are blurring
          href: "#dev-ops-roles-are-blurring"
          data_ga_name: DevOps roles are blurring
          data_ga_location: side-navigation
        - text: Remember to iterate
          href: "#remember-to-iterate"
          data_ga_name: Remember to iterate
          data_ga_location: side-navigation
    content:
      - name: topics-copy-block
        data:
          header: How to think about DevOps team structure
          blocks:
            - column_size: 8
              text: |
                A solid [DevOps platform](/topics/devops-platform/) needs a [solid DevOps team structure](/topics/devops/how-and-why-to-create-devops-platform-team/) to achieve maximum efficiency.

                Several factors come into play when it comes to team structure:

                * Existing silos: Are there product sets/teams that work independently?
                * Technical leadership: Are group managers set up to [achieve DevOps goals](/topics/devops/seven-tips-to-get-the-most-out-of-your-devops-platform/)?
                * Changing roles: Ops tasks have bled into dev roles, security teams are working with everyone, and technology is changing. Expect to regularly re-evaluate everything.
                * Continuous improvement: A DevOps team will never be a “one and done.” Iteration will be required.
      - name: topics-copy-block
        data:
          header: Types of silos
          blocks:
            - column_size: 8
              text: |
                Management consultant Matthew Skelton writes about a number of different DevOps scenarios in great detail, but we’ll discuss just a few of the silos he mentions specifically and how they impact an organization.
      - name: topics-copy-block
        data:
          header: Dev and ops are completely separate
          blocks:
            - column_size: 8
              text: |
                Skelton refers to this as a classic “throw it over the wall” team structure and, as implied, it’s not the most effective DevOps strategy. Both teams work in their bubbles and lack visibility into the workflow of the other team. This complete separation lacks collaboration, visibility, and understanding – vital components of what effective DevOps should be. What happens is essentially blame-shifting: “We don’t know what they are doing over there, we did our part and now it’s up to them to complete it,” and so on.
      - name: topics-copy-block
        data:
          header: DevOps middleman
          blocks:
            - column_size: 8
              text: |
                In this team structure, there are still separate dev and ops teams, but there is now a “DevOps” team that sits between, as a facilitator of sorts. This is not necessarily a bad thing and Skelton stresses that this arrangement has some use cases. For example, if this is a temporary solution with the goal being to make dev and ops more cohesive in the future, it could be a good interim strategy.
      - name: topics-copy-block
        data:
          header: Ops stands alone
          blocks:
            - column_size: 8
              text: |
                In this scenario, dev and DevOps are melded together while ops remains siloed. Organizations like this still see ops as something that supports the initiatives for software development, not something with value in itself. Organizations like this suffer from basic operational mistakes and could be much more successful if they understand the value ops brings to the table.
      - name: topics-copy-block
        data:
          header: What can DevOps team leadership do?
          blocks:
            - column_size: 8
              text: |
                To break down DevOps team silos requires leadership at all levels. Start by asking each group to surface the major areas of friction and then identify leaders in each group – dev, ops, security, test. Each leader should work individually and together on all of the friction points.

                The importance of communication can’t be overstated: Teams need to hear regular feedback about all aspects of their roles.

                It might also be helpful to insert “champions” into struggling groups; they can model behaviors and language that facilitate communication and collaboration.
      - name: topics-copy-block
        data:
          header: DevOps roles are blurring
          blocks:
            - column_size: 8
              text: |
                Technology advances from multicloud to [microservices](/topics/microservices/) and containers also play a role when it comes to defining the right DevOps team structure. In our [2020 Global DevSecOps Survey](/developer-survey/), 83% of respondents said their teams are releasing code more quickly but they also told us their roles were changing, dramatically in some cases.

                Devs today are creating, monitoring, and maintaining infrastructures, roles that were traditionally the province of ops pros. Ops are spending more time managing cloud services, while security team members are working on cross-functional teams with dev and ops more than ever before.

                Obviously the software development lifecycle today is full of moving parts, meaning that defining the right structure for a DevOps team will remain fluid and in need of regular re-evaluation.
      - name: topics-copy-block
        data:
          header: Remember to iterate
          blocks:
            - column_size: 8
              text: |
                At GitLab [iteration](/handbook/values/#iteration) is one of our core values. And it’s something we practice a lot when it comes to our own DevOps team structure. Since GitLab is a complete [DevOps platform](/solutions/devops-platform/) delivered as a single application, our dev teams are organized into stages (e.g. [Verify](/handbook/engineering/development/ops/verify/), etc.) because these would be separate products at any other company and require their own autonomy. We also have other functional DevOps groups besides “Dev” that manage other aspects of our product.

                We have a [reliability group](/handbook/engineering/infrastructure/team/reliability/) that manages uptime and reliability for GitLab.com, a [quality department](/handbook/engineering/quality/), and a [distribution team](/handbook/engineering/development/enablement/systems/distribution/), just to name a few. The way that we make all these pieces fit together is through our commitment to transparency and our visibility through the entire SDLC. But we also tweak (i.e. iterate on) this structure regularly to make everything work.

                The bottom line: Plan to build your DevOps team, and then re-think it, and re-think it some more. The benefits in faster code releases and happier team members will make it worthwhile.
      - name: topics-cta
        data:
          title: Mapping the DevSecOps Landscape
          text: |
            This year, over 3,650 respondents from 21 countries spoke about their DevOps successes, challenges, and ongoing struggles.
          column_size: 10
          cta_one:
            text: Get the Full Report
            link: /developer-survey/
            data_ga_name: Get the Full Report
            data_ga_location: body
  components:
    - name: solutions-resource-cards
      data:
        title: More on DevOps teams
        column_size: 4
        cards:
          - icon:
              name: blog
              variant: marketing
              alt: blog Icon
            event_type: Blog
            header: The developer role is changing. Here's what to expect
            image: /nuxt-images/blogimages/axway-case-study-image.png
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2020/10/20/software-developer-changing-role/
            data_ga_name:
            data_ga_location: body
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: Case Study
            header: Why you need a security champions program
            image: /nuxt-images/blogimages/autodevops.jpg
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2020/10/14/why-security-champions/
            data_ga_name:
            data_ga_location: body
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: Blog
            header: Advance DevOps with communication and collaboration
            image: /nuxt-images/blogimages/scm-ci-cr.png
            link_text: "Learn more"
            href: https://about.gitlab.com/blog/2020/11/23/collaboration-communication-best-practices/
            data_ga_name:
            data_ga_location: body
