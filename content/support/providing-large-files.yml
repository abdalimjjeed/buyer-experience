---
title: Provide large files to GitLab support
description: How to share large files with the GitLab support team.
support-hero:
  data:
    title: Provide large files to GitLab support
    content: |
      Zendesk has a <a href="https://support.zendesk.com/hc/en-us/articles/4408832757146#topic_lv2_cnx_xdb">maximum attachment size</a> of 50MB per file.
      Zendesk will not allow us to increase this limit any further.
side_menu:
  anchors:
    text: 'On this page'
    data:
      - text: 'Support Uploader'
        href: '#support-uploader'
      - text: "Other methods"
        href: "#other-methods"
        nodes:
          - text: 'Compression'
            href: '#compression'
          - text: 'File sharing services'
            href: '#file-sharing-services'
          - text: 'GitLab private project'
            href: '#gitlab-private-project'
          - text: "Use GNU split"
            href: "#use-gnu-split"
  hyperlinks:
    text: ''
    data: []
components:
- name: support-copy-markdown
  data:
    block:
    - header: Support Uploader
      id: support-uploader
      text: |
        A Support Engineer can generate and provide you with FTP credentials for your ticket specifically to allow for the uploading of larger files (that cannot be uploaded via the ticket system). The FTP system will automatically delete these files within an hour of the ticket being marked as Closed.
    - subtitle:
        text: Note
        id: US-federal-note
      text: |
        The support uploader tool is not for use with US Federal Support Portal cases. Please leverage the other methods available or reach out to the support engineer via the case for assistance uploading large files.
    - subtitle:
        text: Support Uploader Usage
        id: support-uploader-usage
      text: |
        **Uploading a file via CLI**
        To upload a file via CLI, access FTP using the `ftp HOST` command (replacing `HOST` with the host given to you by the support engineer). <br>If the command is not available, you might want to install it using a command such as `sudo apt-get ftp` or `brew install inetutils`.
        After running the `ftp` command, you should be asked for the username to use (provided to you by the support engineer on the ticket). <br>After entering that, you would then provide the password (also provided to you by the support engineer on the ticket).
        Once fully logged in, you can upload a file using the FTP `put path/to/file` command (replacing `path/to/file` with the local path to the file you wish to upload). You will also need to ensure files are uploaded to the `/files/` directory on the FTP server.<br>As an example, uploading the local file `Docs/logs.zip` would be done using the FTP command `put Docs/logs.zip files/logs.zip`.
        Once you are done uploading the file, you can exit FTP by using the FTP command `bye`.
        You can also use `curl` to upload a file using FTP via the following command: `curl -T testfile.txt  ftp://<username>:<password>@<host>/files/`.
        **Download a file via CLI**
        To download a file via CLI, access FTP using the `ftp HOST` command (replacing `HOST` with the host given to your by the support engineer). <br>If the command is not available, you might want to install it using a command such as `sudo apt-get ftp` or `brew install inetutils`.
        After running the `ftp` command, you should be asked for the username to use (provided to you by the support engineer on the ticket).<br>After entering that, you would then provide the password (also provided to you by the support engineer on the ticket).
        Once fully logged in, you can upload a file using the FTP `get path/to/file` command (replacing `path/to/file` with the server path to the file you wish to upload). <br>As an example, uploading the server file `files/logs.zip` would be done using the FTP command `get files/logs.zip`.
        Once you are done downloading the file, you can exit FTP by using the FTP command `bye`.
        You can also use `curl` to download a file using FTP via the following command: `curl -o testfile.txt  ftp://<username>:<password>@<host>/files/testfile.txt`.
        **Some other tools you could use**
        While not in any way promoted or supported by GitLab Support, some other FTP tools that are available:

        * [CyberDuck](https://cyberduck.io/)
        * [FileZilla](https://filezilla-project.org/)
    - header: Other methods
      id: other-methods
      text: |
        For legacy reasons, we also list methods that were used in the past to provide large files to us. If you absolutely cannot use the Support Uploader, you can choose one of the following to share your files.
    - subtitle:
        text: Compression
        id: compression
      text: |
        If you're sending a text file or an archive with mostly text files, then please compress it. Use either bz2 (preferred) or gzip (faster) compression and it should compress to a small percentage of its original size. Zip compression is fine if you're on Windows.
        If this brings your file under the 50MB limit, then simply attach it to the ticket and be done. If not, then see below for more options.
    - subtitle:
        text: File sharing services
        id: file-sharing-services
      text: |
        Please feel free to use your own choice of file sharing service. Be aware that submitting potentially sensitive data to 3rd parties does carry a risk, so be sure to check with your security team for a properly vetted choice.
    - subtitle:
        text: GitLab private project
        id: gitlab-private-project
      text: |
        This is a fairly straight-forward option. [Sign up for a gitlab.com account](https://gitlab.com/users/sign_in#register-pane) if you don't already have one. Then create a private project and invite the Support Engineer(s) assisting you with **Reporter** access or higher. You can find the GitLab handle of the support engineer(s) you need to add by searching for the respective engineer(s) on our [team](/company/team/) page.
    - subtitle:
        text: Use GNU split
        id: use-gnu-split
      text: |
        Since the attachment is applied per file, we can split that one file into many and attach all of them to a ticket.
        The `split` command is bundled in GNU coreutils, which should be installed on all Unix-like operating systems by default. Please avoid using alternatives like winzip, winrar, 7zip, etc. We've included an example below:

        > `split -b <size> <source file> <prefix for new files>`
        > `split -b 19M source-file.tar.bz2 "target-file.tar.bz2."s`

        This will create many files in your current directory such as `target-file.tar.bz2.aa` and `target-file.tar.bz2.ab.` These files can be later joined with the `cat` command.

        > `cat target-file.tar.bz2.* > joined-file.tar.bz2`
