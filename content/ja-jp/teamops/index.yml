---
  title: 'TeamOps: チーム効率の最適化 | GitLab'
  og_title: 'TeamOps: チーム効率の最適化 | GitLab'
  description: TeamOps は、結果重視のチーム管理規律であり、意思決定の阻害要因を減らして、迅速かつ効率的な戦略実行を保証します。 もっと詳しく知る！
  twitter_description: TeamOps は、結果重視のチーム管理規律であり、意思決定の阻害要因を減らして、迅速かつ効率的な戦略実行を保証します。 もっと詳しく知る！
  og_description: TeamOps は、結果重視のチーム管理規律であり、意思決定の阻害要因を減らして、迅速かつ効率的な戦略実行を保証します。 もっと詳しく知る！
  og_image: /nuxt-images/open-graph/teamops-opengraph.png
  twitter_image: /nuxt-images/open-graph/teamops-opengraph.png
  hero:
    logo:
      show: true
    title: |
      より良いチーム。
      より速い進行。
      より良い世界。
    subtitle: チームワークを客観的な規律にする
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 200
    image:
      url: /nuxt-images/team-ops/hero-illustration.png
      alt: チームオプスのヒーロー画像
      aos_animation: zoom-out-left
      aos_duration: 1600
      aos_offset: 200
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: チームを登録する
      data_ga_name: enroll your team
      data_ga_location: body
  spotlight:
    title: |
      TeamOps は意思決定速度を促進します
    subtitle: TeamOps は、意思決定、情報、およびタスクをより効率的に管理することにより、チームが生産性、柔軟性、および自律性を最大化するのに役立つ組織運用モデルです。
    description:
      "意思決定を改善し、その実行を改善するための環境を作ることは、チームの改善につながり、最終的には進歩につながります。\n\n\n
       TeamOps は、GitLab が 10 年でスタートアップからグローバルな上場企業に成長した方法です。 現在、すべての組織に公開しています。"
    list:
      title: 一般的な問題点
      items:
        - 意思決定の遅れ
        - 会議疲れ
        - 内部の誤解
        - 遅いハンドオフとワークフローの遅延
      icon: warning
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: チームをより良くする
      data_ga_name: make your team better
      data_ga_location: body
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  features:
    title: |
      すべてのチームのために構築されました。
      リモート、ハイブリッド、またはオフィス。
    image:
      url: /nuxt-images/team-ops/reasons-to-believe.png
      alt: Team Ops のイメージを信じる理由
    accordion:
      is_accordion: false
      items:
        - icon:
            name: group
            alt: User Group Icon
            variant: marketing
          header: TeamOps は、新しい目標、結果重視の管理規律です
          text: TeamOps は、チーム メンバーの関係を運用可能な問題として扱うことにより、組織がより大きな進歩を遂げるのに役立ちます。
        - icon:
            name: clipboard-check-alt
            alt: Clipboard Checkmar Icon
            variant: marketing
          header: フィールドテスト済みのシステム
          text: 過去 5 年間、GitLab で TeamOps を構築して使用してきました。 その結果、組織の生産性が向上し、チーム メンバーの仕事への満足度が向上しています。 ここで作成されましたが、ほぼすべての組織に役立つと信じています。
        - icon:
            name: principles
            alt: Continuous Integration Icon
            variant: marketing
          header: 指導原則
          text: TeamOps は、組織が動的で変化する仕事の性質を合理的にナビゲートするのに役立つ 4 つの指導原則に基づいています。
        - icon:
            name: cog-user-alt
            alt: Cog User Icon
            variant: marketing
          header: アクションの信条
          text: 各原則は一連の行動原則によってサポートされています。これは、すぐに実装できる行動ベースの作業方法です。
        - icon:
            name: case-study-alt
            alt: Case Study Icon
            variant: marketing
          header: 実際の例
          text: 私たちは、Gitlab で実践されているこのテネットの実際の出力ベースの例のライブラリを増やして、アクションのテネットに命を吹き込みます。
        - icon:
            name: verification
            alt: Ribbon Check Icon
            variant: marketing
          header: チームオプスコース
          text: チームや企業が共有環境でフレームワークを体験できるように、TeamOps コースを通じて調整を行います。
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
    accordion_aos_animation: fade-left
    accordion_aos_duration: 1600
    aaccordion_os_offset: 200
  video_spotlight:
    title: |
      TeamOps に没頭する
    subtitle: 世界には、仕事の未来について多くの意見があります。
    description:
      "TeamOps コースは、チーム メンバーの関係を運用可能な問題として扱うことにより、組織がより大きな進歩を遂げるのに役立ちます。\n\n\n
       わずか数時間で、モデルの指導原則と行動原則のそれぞれを深く掘り下げ、チームの現在の運用慣行との互換性を評価し始めることができます。"
    video:
      url: 754916142?h=56dd8a7d5d
      alt: チームオプスのヒーロー画像
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: 今すぐ登録
      data_ga_name: enroll your team
      data_ga_location: body
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  card_section:
    title: |
      TeamOps の基本原則
    subtitle: 組織には、人々とチーム、つまり創造性、視点、人間性が必要です。
    cards:
      - title: 共有現実
        description: |
          他の管理哲学が知識伝達の速度を優先するのに対し、TeamOps は知識検索の速度を最適化します。
        icon:
          name: d-and-i
          slp_color: surface-700
        link:
          text: もっと詳しく知る
          url: /handbook/teamops/shared-reality/
        color: '#FCA326'
      - title: 誰もが貢献する
        description: |
          組織は、レベル、機能、または場所に関係なく、誰もが情報を消費して貢献できるシステムを作成する必要があります。
        icon:
          name: user-collaboration
          slp_color: surface-700
        link:
          text: もっと詳しく知る
          url: /handbook/teamops/everyone-contributes/
        color: '#966DD9'
      - title: 決定速度
        description: |
          成功は意思決定速度と相関があります。つまり、特定の時間内に行われた意思決定の量と、より速い進歩から生じる結果です。
        icon:
          name: speed-alt-2
          slp_color: surface-700
        link:
          text: もっと詳しく知る
          url: /handbook/teamops/decision-velocity/
        color: '#FD8249'
      - title: 測定の明瞭さ
        description: |
          これは、正しいものを測定することです。 TeamOps の意思決定の原則は、実行して結果を測定する場合にのみ役立ちます。
        icon:
          name: target
          slp_color: surface-700
        link:
          text: もっと詳しく知る
          url: /handbook/teamops/measurement-clarity/
        color: '#256AD1'
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  join_us:
    title: |
      運動に参加する
    description:
      "TeamOps は、意思決定、情報、およびタスクをより効率的に管理することにより、チームが生産性、柔軟性、および自律性を最大化するのに役立つ組織運用モデルです。 \n\n\n
       TeamOps を実践している組織のリストに参加してください。"
    list:
      title: 一般的な問題点
      items:
        - アドホックなワークフローが調整を妨げる
        - DIY管理は機能不全を生む
        - 通信インフラは後付け
        - コンセンサスへの執着がイノベーションを妨げる
      icon: warning
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: 今すぐ登録
      data_ga_name: make your team better
      data_ga_location: body
    quotes:
      - text: 展開前のテストにより、製品をリリースする準備ができているという確信が得られました。 配信頻度も増えました。
        author: ジョンの姓
        note: 取締役の役職、会社名
      - text: 展開前のテストにより、製品をリリースする準備ができているという確信が得られました。 配信頻度も増えました。
        author: ジョンの姓
        note: 取締役の役職、会社名
    clients:
      - logo: /nuxt-images/home/logo_cncf_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_goldman_sachs_mono.svg
        alt: Goldman Sachs logo
      - logo: /nuxt-images/home/logo_siemens_mono.svg
        alt: Siemens logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/team-ops/logo_knowbe4_mono.svg
        alt: KnowBe4 logo
      - logo: /nuxt-images/home/logo_cncf_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_goldman_sachs_mono.svg
        alt: Goldman Sachs logo
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
