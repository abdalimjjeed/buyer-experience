---
  title:  ソフトウェア。 もっと早く
  description: GitLab は DevSecOps を簡素化するため、重要なことに集中できます。 もっと詳しく知る！
  image_title: /nuxt-images/open-graph/open-graph-gitlab.png
  twitter_image: /nuxt-images/open-graph/open-graph-gitlab.png
  hero:
    title: |
      アイデアからソフトウェアへのより速いパス
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 0
    image:
      url: /nuxt-images/software-faster/hero.png
      alt: software-faster hero image
      aos_animation: fade-up
      aos_duration: 1600
      aos_offset: 0
    secondary_button:
      video_url: https://player.vimeo.com/video/799236905?h=59b06b0e99&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
      text: GitLabとは？
      data_ga_name: watch video
      data_ga_location: hero
  customer_logos:
    showcased_enterprises:
      - image_url: "/nuxt-images/home/logo_tmobile_mono.svg"
        link_label: T-Mobile と GitLab の Web キャストのランディング ページへのリンク
        alt: "T-Mobile logo"
        url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: Link zur Kundenfallstudie von Goldman Sachs
        alt: "Goldman Sachs logo"
        url: "/customers/goldman-sachs/"
      - image_url: "/nuxt-images/home/logo_cncf_mono.svg"
        link_label: Cloud Native Computing Foundation のお客様のケース スタディへのリンク
        alt: "Cloud Native logo"
        url: /customers/cncf/
      - image_url: "/nuxt-images/home/logo_siemens_mono.svg"
        link_label: シーメンスのお客様事例へのリンク
        alt: "Siemens logo"
        url: /customers/siemens/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: Nvidia のお客様のケース スタディへのリンク
        alt: "Nvidia logo"
        url: /customers/nvidia/
      - alt: UBS Logo
        image_url: "/nuxt-images/home/logo_ubs_mono.svg"
        url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
        link_label: UBS のお客様事例へのリンク
  featured_content:
    col_size: 4
    header: "一緒により良い: お客様は GitLab を使用してソフトウェアをより迅速に出荷します"
    case_studies:
      - header: Nasdaq の迅速でシームレスなクラウドへの移行
        description: |
          Nasdaq は 100% クラウドになるというビジョンを持っています。 彼らはそこに到達するために GitLab と提携しています。
        showcase_img:
          url: /nuxt-images/software-faster/nasdaq-showcase.png
          alt: Nasdaq logo on a window
        logo_img:
          url: /nuxt-images/enterprise/logo-nasdaq.svg
          alt: Nasdaq logo
        link:
          video_url: https://player.vimeo.com/video/767082285?h=e76c380db4
          text: ビデオを見る
          data_ga_name: nasdaq
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 800
      - header: 展開時間を 5 倍高速化
        description: |
          Hackerone は、GitLab Ultimate を使用して、パイプラインの時間、デプロイの速度、および開発者の効率を改善しました。
        showcase_img:
          url: /nuxt-images/software-faster/hackerone-showcase.png
          alt: Person working on a computer with code - HackerOne
        logo_img:
          url: /nuxt-images/logos/hackerone-logo.png
          alt: HackerOne logo
        link:
          href: /customers/hackerone/
          text: もっと詳しく知る
          data_ga_name: hackerone
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1000
      - header: リリース機能の 144 倍の速さ
        description: |
          Airbus Intelligence は、単一アプリケーション CI でワークフローとコード品質を改善しました。
        showcase_img:
          url: /nuxt-images/software-faster/airbus-showcase.png
          alt: Airplane wing on flight - Airbus
        logo_img:
          url: /nuxt-images/software-faster/airbus-logo.png
          alt: Airbus logo
        link:
          href: /customers/airbus/
          text: 彼らのストーリーを読む
          data_ga_name: airbus
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1200
  devsecops:
    header: 1 つの包括的なプラットフォームに不可欠なすべての DevSecOps ツール
    link:
      text: もっと詳しく知る
      data_ga_name: platform
      data_ga_location: body
    cards:
      - header: より良い洞察
        description: ソフトウェア配信ライフサイクル全体にわたるエンドツーエンドの可視性。
        icon: case-study-alt
      - header: 効率の向上
        description: 自動化とサード サービスとの統合の組み込みサポート。
        icon: principles
      - header: コラボレーションの改善
        description: 開発者、セキュリティ、運用チームを統合する 1 つのワークフロー。
        icon: roles
      - header: 価値実現までの時間の短縮
        description: 加速されたフィードバック ループによる継続的な改善。
        icon: verification
  by_industry_case_studies:
    title: DevSecOps リソースを調べる
    charcoal_bg: true
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: 2023 年グローバル DevSecOps レポート シリーズ
        subtitle: ソフトウェア開発、セキュリティ、運用の現状について、5,000 人を超える DevSecOps の専門家から学んだことをご覧ください。
        image:
          url: /nuxt-images/software-faster/devsecops-survey.svg
          alt: devsecops survey icon
        button:
          href: /developer-survey/
          text: レポートを読む
          data_ga_name: devsecops survey
          data_ga_location: body
        icon:
          name: doc-pencil-alt
          alt: Play Circle Icon
      - title: 'ケイデンスがすべて: 10 倍のエンジニアのための 10 倍のエンジニアリング組織。'
        subtitle: GitLab の CEO で共同設立者の Sid Sijbrandij が、エンジニアリング組織におけるケイデンスの重要性について語っています。
        image:
          url: /nuxt-images/blogimages/athlinks_running.jpg
          alt: picture of people running marathon
        button:
          href: /blog/2022/11/03/cadence-is-everything-10x-engineering-organizations-for-10x-engineers/
          text: ブログを読む
          data_ga_name: read the blog
          data_ga_location: body
        icon:
          name: blog
          alt: Play Circle Icon
