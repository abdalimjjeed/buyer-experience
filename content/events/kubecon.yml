metadata:
  title: GitLab + KubeCon EU 2023
  og_title: GitLab + KubeCon EU 2023
  description: "The GitLab team is ready for three action-packed days of in-person awesomeness. Here are some ways to hang out with us at KubeCon EU"
  twitter_description: "The GitLab team is ready for three action-packed days of in-person awesomeness. Here are some ways to hang out with us at KubeCon EU"
  image_title: /nuxt-images/open-graph/gitlab-kubecon-opengraph.png
  og_description: "The GitLab team is ready for three action-packed days of in-person awesomeness. Here are some ways to hang out with us at KubeCon EU"
  og_image: /nuxt-images/open-graph/gitlab-kubecon-opengraph.png
  twitter_image: /nuxt-images/open-graph/gitlab-kubecon-opengraph.png

hero:
  headline: What’s GitLab up to at KubeCon + CloudNativeCon Europe 2023?
  accent: GitLab + KubeCon EU 2023
  sub_heading: Join the GitLab team for…
  list:
  - icon: bulb-bolt-96px.svg
    description: Exciting lightning talks throughout the week in our booth!
  - icon: code-96px.svg
    description: Try your hand at our Coding Challenge
  - icon: celebrate-icon_celebrate-96px.svg
    description: Late Night DevApps & Brews
  - icon: tee-96px.svg
    description: Exclusive GitLab swag

intro:
  headline: We can't wait to see you in Amsterdam!
  description: The GitLab team is ready for three action-packed days of in-person awesomeness.
                Here are some ways to hang out with us at KubeCon EU!

gitlab_booth:
  title: GitLab Booth
  subheadings:
    - title: Location
      value: P1
  information: "Come by the GitLab booth and take a deep dive into our DevSecOps Platform, learn best practices from product experts, get your technical questions answered, grab some swag, and let us know what you'd like to see in new releases. That's not all, we will have various lightning talks all week in our booth. Here's a sneak peek at some of our talks:"
  image:
    src: "387A1087-compressed 1.jpg"
    alt: ''

featured_lighting_talks:
  title: Featured Lightning Talks
  layout: spaced
  list:
    - title: Identifying areas of improvement with DORA metrics
      speaker: Cesar Saavedra, Senior Technical Marketing Manager at GitLab
    - title: GitLab CI/CD pipelines
      speaker: Itzik Gan Baruch, Senior Technical Marketing Manager at GitLab
    - title: Ensuring Software Supply-Chain Security with GitLab
      speaker: Fernando Diaz, Senior Technical Marketing Manager at GitLab
    - title: Embed Security and testing in your Machine Learning Developer Workflows
      speaker: William Arias, Senior Technical Marketing Manager at GitLab
    - title: Efficient DevSecOps Pipelines in a Cloud-Native World
      speaker: Michael Friedrich, Senior Developer Evangelist  at GitLab
    - title: Benefits of bringing your career to GitLab
      speaker: Joanna Michniewicz, Recruiter, Support Engineering at GitLab
    - title: Retail edge solutions with Google Cloud Platform and GitLab
      speaker: Regnard Raquedan, Alliances Solutions Architect at GitLab & Mike Ensor, Senior Distributed Edge Solutions Architect at Google
    - title: Deploying Apps to GKE with Auto DevOps
      speaker: Regnard Raquedan, Alliances Solutions Architect at GitLab & Nick Eberts, GKE Product Manager at Google
    - title: Value Stream Assessment at GitLab
      speaker: Andrea Obermeier, Manager, Solutions Architects at GitLab

party:
  title: Stop by for Late Night DevApps & Brews at the Heineken Experience!
  icon: celebrate-24px.svg
  subheadings:
    - title: Location
      value: 'Heineken Experience'
    - title: Address
      value: 'Stadhouderskade 78, 1072 AE Amsterdam, Netherlands'
    - title: Date
      value: 'April 19, 2023'
    - title: Time
      value: '9PM - 12AM CEST'
  information: |
              Welcome and get ready for some Late Night DevApps & Brews at the Heineken Experience! Join us after the booth crawl for an evening filled with food, drinks, music, and networking opportunities with fellow KubeCon attendees.
  note: '** Please note this venue has a limited capacity. Preregistration is required and admittance is provided on a first-come, first-served basis until capacity is met.'
  call_to_action:
    button_text: Register here
    button_link: https://www.eventbrite.com/e/kubecon-cloudnativecon-eu-2023-late-night-devapps-brews-tickets-579354293287
  image:
    src: "gitlab-devapps-brew-landing-1080x612.png"
    alt: ''
    class: cropped

schedule:
  title: Attend a Live Demo or Lightning Talk at our Mini Theater
  description: Stop by the GitLab booth for a jam-packed schedule of hourly lightning talks from our DevOps experts on a number of engaging topics. Check out the schedule.
  schedule_list:
    - date: Wednesday, April 19
      events:
      - title: Value Stream Assessment at GitLab 
        speaker: Andrea Obermeier, Manager, Solutions Architects at GitLab 
      - title: Enterprise DevSecOps with Nutanix and GitLab
        speaker: Cesar Saavedra, Senior Technical Marketing Manager at GitLab & Gautier Leblanc, Portfolio Products Specialist at Nutanix
      - title: Better Progressive Delivery with Dynatrace Site Reliability Guardian
        speaker: Kristof Renders, Director of Innovative Services at Dynatrace
      - title: How Krateo, as a Platform Engineering solution, enables GitLab to build and deliver safe, scalable and efficient software
        speaker: John Feeney, Solutions Architect at GitLab & Diego Braga, Krateo Product Manager at Kiratech
      - title: Securing applications in the pipeline with OpenClarity and Panoptica
        speaker: Tim Miller, Technical Marketing Engineer at Cisco
      - title: Enterprise Data Protection for Cloud Native Hybrid GitLab
        speaker: Michael Haigh, Senior Technical Marketing Engineer at NetApp
      - title: 'Rise of Protestware: How to stay one step ahead'
        speaker: Abubakar Siddiq Ango, Developer Evangelism Program Manager at GitLab
      - title: Identifying areas of improvement with DORA metrics
        speaker: Cesar Saavedra, Senior Technical Marketing Manager at GitLab
      - title: Ensuring Software Supply-Chain Security with GitLab
        speaker: Fernando Diaz, Senior Technical Marketing Manager at GitLab
      - title: Efficient DevSecOps Pipelines in a Cloud-Native World
        speaker: Michael Friedrich, Senior Developer Evangelist at GitLab

    - date: Thursday, April 20th
      events:
      - title: Would you build an email server from scratch in a retail company?
        speaker: Andy Allred, Host of The DevOps Sauna Podcast, Platform Engineering at Eficode
      - title: Kubernetes as a Service with GitOps - The CAPI-move towards cloud-native
        speaker: Kim-Norman Sahm, Principal Sales Engineer at D2iQ
      - title: Deploying Apps to GKE with Auto DevOps
        speaker: Regnard Raquedan, Alliances Solutions Architect at GitLab & Nick Eberts, GKE Product Manager at Google
      - title: Benefits of bringing your career to GitLab
        speaker: Joanna Michniewicz, Recruiter, Support Engineering at GitLab
      - title: Compliance Management with GitLab
        speaker: Nupur Sharma, Solution Architect at GitLab
      - title: GitLab CI/CD pipelines
        speaker: Itzik Gan Baruch, Senior Technical Marketing Manager at GitLab

    - date: Friday, April 21st
      events:
      - title: Retail edge solutions with Google Cloud Platform and GitLab
        speaker: Regnard Raquedan, Alliances Solutions Architect at GitLab & Mike Ensor, Senior Distributed Edge Solutions Architect at Google
      - title: Embed Security and testing in your Machine Learning Developer Workflows
        speaker: William Arias, Senior Technical Marketing Manager at GitLab
      - title: How to navigate the interview process
        speaker: Mark Deubel, Senior Technical Recruiter, Development at GitLab & Matt Angell, Senior Technical Recruiter at GitLab
      - title: Domain Driven Development with Gitlab, Products are more important than tools
        speaker: Stefan Gärtner, Partner Manager at SVA

careers:
  cta:
    href: https://about.gitlab.com/jobs/all-jobs/
    text: View open roles at GitLab
