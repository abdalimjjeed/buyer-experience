---
title: Probieren Sie GitLab kostenlos aus
description: Genießen Sie die kostenlose 30-Tage-Testversion von GitLab Ultimate und erleben Sie den gesamten Lebenszyklus der Softwareentwicklung und das DevOps-Tool mit einer umfangreichen Palette innovativer Funktionen.
components:
  - name: call-to-action
    data:
      title: Testen Sie GitLab Ultimate 30 Tage lang kostenlos
      centered_by_default: true
      subtitle: Die kostenlose Testversion umfasst fast[[1]](#what-is-included-in-my-free-trial-what-is-excluded){class="cta__subtitle--subscript"} alle [Ultimate-tier]( /pricing/ultimate/){data-ga-name="Kostenlose Testversion enthält fast alle data-ga-location="header"}-Funktionen der Ultimate-Stufe. Keine Kreditkarte erforderlich [[2]](/pricing/#why-do-i-need-to-enter-credit-debit-card-details-for-free-pipeline-minutes){class="cta__subtitle--subscript" data-ga-name="Kreditkarten-FAQ" data-ga-location="header"}
      body_text: Starten Sie jetzt Ihre 30-Tage-Testversion. Genießen Sie danach GitLab Free (für immer!).
      aos_animation: fade-down
      aos_duration: 500
  - name: free-trial-plans
    data:
      saas:
        tooltip_text: |
          Software as a Service ist ein Softwarelizenzierungs- und Bereitstellungsmodell, bei dem Software auf Abonnementbasis lizenziert und zentral gehostet wird.
        text: |
          Wir hosten. Keine technische Einrichtung erforderlich. Legen Sie sofort los – keine Installation erforderlich.
        link:
          text: Fahren Sie mit SaaS fort
          href: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=free-trial
          data_ga_name: continue with saas
          data_ga_location: body
        bottom_text: |
          Sie haben bereits ein Konto? [Log In](https://gitlab.com/users/sign_in?redirect_to_referer=yes&__cf_chl_jschl_tk__=db2d336ba94805d0675008cc3fa3e0882d90953c-1619131501-0-AeQCSleOFTDGa9C-lXa3ZZZPpsO6sh0lCBCPZT0GxdT7tyOMAZoPzKppSQq9eV2Gqq9_kwKB8Lt8GJQ-nF-ra8updJRDfWTMBAwCR-m38kaHdAJYTicvW8Tj4KH55GO25zOeCYJexeEp1hx6f3DMvtjZd8elp_RfdulgN4-rxW8-lFSumJdSzE8y8N9FGltpsoQ8SKFSq41jMoB_GJ1nkIrjCU_kaGxJA3l4xhh-C14XFoBoBtfGjGOH4Kj76Y5QAeT7qemwuGBlvpYCK0OBv5aPkFDZ_Knp0W1zaOkr5tt511fra-rE3ekQI_lwR5VqBTHLtNslfgt4Il1SKLi6ZJLkces_WsUWdIQ3jNlyKbv08CF6kyDI3NiEOcCXUopCfQDYr-5syEUhv1Cnxy-Vjn7u5ejR2pvwIytWm8io2rhcaSOYxzxWccpxZLfjotTkzlrNP7KALbkxQOcNa_zeWVQ5t6aGC8H5wrT8u8ICxuJC){data-ga-name="anmelden" data-ga-location="body"}
      self_managed:
        form_id: 3648
        form_header: Du beherbergst. Laden Sie GitLab herunter und installieren Sie es auf Ihrer eigenen Infrastruktur oder in einer öffentlichen Cloud-Umgebung. Erfordert Linux-Erfahrung.
  - name: faq
    data:
      aos_animation: fade-up
      aos_duration: 500
      header: Häufig gestellte Fragen (FAQs) zur GitLab-Testversion
      groups:
        - header: Was ist in einer kostenlosen Testversion enthalten
          questions:
            - question: Was ist in meiner kostenlosen Testversion enthalten? Was ist ausgeschlossen?
              answer: |
                Ihre kostenlose Testversion umfasst fast alle Funktionen unserer [ultimativen Stufe](/Preisgestaltung){data-ga-name=\"ultimate tier\" data-ga-location="faq"}, mit diesen Ausnahmen: 
                * Kostenlose Testversionen ausgeschlossen Unterstützung auf jeder Ebene. Wenn Sie GitLab speziell im Hinblick auf Support-Expertise oder SLA-Leistung bewerten möchten, [wenden Sie sich bitte an den Vertrieb](/sales/){data-ga-name="sales" data-ga-location=\"faq\"}, um Optionen zu besprechen.
                * Kostenlose SaaS-Testversionen sind auf 400 CI/CD-Minuten pro Monat begrenzt.
                * Bei SaaS beinhalten kostenlose Testversionen nicht die Verwendung von [Projekttokens](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html).
                * Bei SaaS beinhalten kostenlose Testversionen nicht die Verwendung von [Gruppen-Tokens](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html).
            - question: Was passiert nach Ablauf meiner kostenlosen Testversion?
              answer: |
                Ihre GitLab Ultimate-Testversion dauert 30 Tage. Nach diesem Zeitraum können Sie für immer ein kostenloses GitLab-Konto behalten oder auf einen [kostenpflichtigen Plan](/pricing/){data-ga-name="kostenpflichtiger Plan" data-ga-location="faq"} upgraden.
        - header: SaaS vs. selbstverwaltet
          questions:
            - question: Was ist der Unterschied zwischen SaaS- und selbstverwalteten Setups?
              answer: |
                SaaS: Wir hosten. Es ist keine technische Einrichtung erforderlich, sodass Sie sich nicht selbst um das Herunterladen und Installieren von GitLab kümmern müssen. Selbstverwaltet: Sie hosten. Laden Sie GitLab herunter und installieren Sie es auf Ihrer eigenen Infrastruktur oder in unserer Public Cloud-Umgebung. Erfordert Linux-Erfahrung.
            - question: Sind bestimmte Funktionen nur in SaaS oder Self-Managed enthalten?
              answer: |
                Bestimmte Funktionen sind nur bei Self-Managed verfügbar. Vergleichen Sie die [vollständige Liste der Funktionen hier](/pricing/feature-comparison/){data-ga-name="features list" data-ga-location="faq"}.
        - header: Preise und Rabatte
          questions:
            - question: Ist für eine kostenlose Testversion eine Kredit-/Debitkarte erforderlich?
              answer: |
                Eine Kredit-/Debitkarte ist nicht erforderlich für Kunden, die GitLab.com CI/CD nicht verwenden, ihre eigenen Läufer mitbringen oder gemeinsam genutzte Läufer deaktivieren. Kredit-/Debitkartendaten sind jedoch erforderlich, wenn Sie sich für die Verwendung von GitLab.com Shared Runnern entscheiden. Diese Änderung wurde vorgenommen, um den Missbrauch der auf GitLab.com bereitgestellten kostenlosen Pipeline-Minuten zum Schürfen von Kryptowährungen zu verhindern, was zu Leistungsproblemen für Benutzer von GitLab.com führte. Wenn Sie die Karte bereitstellen, wird sie mit einer Ein-Dollar-Autorisierungstransaktion verifiziert. Es werden keine Gebühren erhoben und es wird kein Geld überwiesen. Weitere Informationen finden Sie [hier](/blog/2021/05/17/prevent-crypto-mining-abuse/){data-ga-name="krypto-mining-missbrauch verhindern" data-ga-location="faq"}.
            - question: Wie viel kostet eine GitLab-Lizenz?
              answer: |
                Abonnementinformationen sind auf unserer [Preisseite](/pricing/){data-ga-name="Preisseite" data-ga-location="faq"} aufgeführt.
            - question: Haben Sie Sonderpreise für Open-Source-Projekte, Startups oder Bildungseinrichtungen?
              answer: |
                Ja! Wir stellen qualifizierten Open-Source-Projekten, Start-ups und Bildungseinrichtungen kostenlose Ultimate-Lizenzen zur Verfügung. Erfahren Sie mehr, indem Sie unser [GitLab for Open Source](/solutions/open-source/){data-ga-name=\"gitlab for open source\" data-ga-location=\"faq\"}, [GitLab for Startups](/solutions/startups/){data-ga-name=\"gitlab for startups\" data-ga-location="faq"} und [GitLab for Education](/solutions/education/){data-ga-name="gitlab for education" data-ga-location="faq"} Programmseiten.
            - question: Wie aktualisiere ich von GitLab Free auf eines der kostenpflichtigen Abonnements?
              answer: |
                Wenn Sie von GitLab Free auf eine der kostenpflichtigen Stufen upgraden möchten, folgen Sie den [Anleitungen in unserer Dokumentation](https://docs.gitlab.com/ee/update/#community-to-enterprise-edition){data-ga-name="Community zu Unternehmen" data-ga-location="faq"}.
        - header: Installation und migration
          questions:
            - question: Wie migriere ich von einem anderen Git-Tool zu GitLab?
              answer: |
                Alle Anweisungen zur Projektmigration für gängige Versionskontrollsysteme finden Sie in [unserer Dokumentation](https://docs.gitlab.com/ee/user/project/import/index.html){data-ga-name="migration" data-ga-location="faq"}.
            - question: Wie installiere ich GitLab über einen Container?
              answer: |
                Informieren Sie sich über die Installation von GitLab mit Docker in [unserer Dokumentation](https://docs.gitlab.com/omnibus/docker/README.html){data-ga-name="install Docker" data-ga-location="faq"}.
        - header: GitLab-Integrationen
          questions:
            - question: Ist GitHost noch verfügbar?
              answer: |
                Nein, wir akzeptieren keine neuen Kunden mehr für GitHost.
            - question: Mit welchen Tools lässt sich GitLab integrieren?
              answer: |
                GitLab bietet eine Reihe von Integrationen von Drittanbietern an. Erfahren Sie mehr über verfügbare Dienste und deren Integration in [unserer Dokumentation](https://docs.gitlab.com/ee/integration/README.html){data-ga-name="third Parteiintegration" data-ga-location="faq"}.
