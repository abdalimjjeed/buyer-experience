---
  title: 'TeamOps: Optimierung der Teameffizienz | GitLab'
  og_title: 'TeamOps: Optimierung der Teameffizienz | GitLab'
  description: TeamOps ist eine ergebnisorientierte Teammanagement-Disziplin, die Entscheidungsblocker reduziert, um eine schnelle und effiziente strategische Umsetzung zu gewährleisten. Erfahren Sie mehr!
  twitter_description: TeamOps ist eine ergebnisorientierte Teammanagement-Disziplin, die Entscheidungsblocker reduziert, um eine schnelle und effiziente strategische Umsetzung zu gewährleisten. Erfahren Sie mehr!
  og_description: TeamOps ist eine ergebnisorientierte Teammanagement-Disziplin, die Entscheidungsblocker reduziert, um eine schnelle und effiziente strategische Umsetzung zu gewährleisten. Erfahren Sie mehr!
  og_image: /nuxt-images/open-graph/teamops-opengraph.png
  twitter_image: /nuxt-images/open-graph/teamops-opengraph.png
  hero:
    logo:
      show: true
    title: |
      Bessere Mannschaften.
      Schnellerer Fortschritt.
      Bessere Welt.
    subtitle: Teamarbeit zu einer objektiven Disziplin machen
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 200
    image:
      url: /nuxt-images/team-ops/hero-illustration.png
      alt: team ops hero image
      aos_animation: zoom-out-left
      aos_duration: 1600
      aos_offset: 200
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Melden Sie Ihr Team an
      data_ga_name: Melden Sie Ihr Team an
      data_ga_location: body
  spotlight:
    title: |
      TeamOps treibt die Entscheidungsgeschwindigkeit voran
    subtitle: TeamOps ist ein organisatorisches Betriebsmodell, das Teams hilft, Produktivität, Flexibilität und Autonomie zu maximieren, indem Entscheidungen, Informationen und Aufgaben effizienter verwaltet werden.
    description:
      "Die Schaffung einer Umgebung für bessere Entscheidungen und deren verbesserte Ausführung führt zu besseren Teams – und letztendlich zu Fortschritt.\n\n\n
       TeamOps ist die Art und Weise, wie GitLab innerhalb eines Jahrzehnts von einem Startup zu einem globalen Aktienunternehmen skaliert wurde. Jetzt öffnen wir es für jede Organisation."
    list:
      title: Häufige Schmerzpunkte
      items:
        - Verzögerungen bei der Entscheidungsfindung
        - Müdigkeit begegnen
        - Interne Fehlkommunikation
        - Langsame Übergaben und Workflow-Verzögerungen
      icon: warning
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Machen Sie Ihr Team besser
      data_ga_name: make your team better
      data_ga_location: body
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  features:
    title: |
      Gebaut für alle Teams.
      Remote, Hybrid oder Office.
    image:
      url: /nuxt-images/team-ops/reasons-to-believe.png
      alt: Reasons to Believe in Team Ops image
    accordion:
      is_accordion: false
      items:
        - icon:
            name: group
            alt: User Group Icon
            variant: marketing
          header: TeamOps ist eine neue zielorientierte, ergebnisorientierte Managementdisziplin
          text: TeamOps hilft Organisationen dabei, größere Fortschritte zu erzielen, indem es die Beziehung Ihrer Teammitglieder als Problem behandelt, das operationalisiert werden kann.
        - icon:
            name: clipboard-check-alt
            alt: Clipboard Checkmar Icon
            variant: marketing
          header: Ein praxiserprobtes System
          text: Wir haben TeamOps bei GitLab in den letzten 5 Jahren entwickelt und verwendet. Infolgedessen ist unsere Organisation produktiver und unsere Teammitglieder zeigen eine größere Arbeitszufriedenheit. Es wurde hier erstellt, aber wir glauben, dass es fast jeder Organisation helfen kann.
        - icon:
            name: principles
            alt: Continuous Integration Icon
            variant: marketing
          header: Leitprinzipien
          text: TeamOps basiert auf vier Leitprinzipien, die Organisationen helfen können, die dynamische, sich verändernde Natur der Arbeit rational zu steuern.
        - icon:
            name: cog-user-alt
            alt: Cog User Icon
            variant: marketing
          header: Handlungsgrundsätze
          text: Jedes Prinzip wird durch eine Reihe von Aktionsgrundsätzen unterstützt – verhaltensbasierte Arbeitsweisen, die sofort umgesetzt werden können.
        - icon:
            name: case-study-alt
            alt: Case Study Icon
            variant: marketing
          header: Beispiele aus der Praxis
          text: Wir erwecken die Aktionsgrundsätze mit einer wachsenden Bibliothek realer, ergebnisbasierter Beispiele dieses Grundsatzes zum Leben, wie sie bei Gitlab praktiziert werden.
        - icon:
            name: verification
            alt: Ribbon Check Icon
            variant: marketing
          header: TeamOps-Kurs
          text: Wir schaffen Ausrichtung durch den TeamOps-Kurs, der es Teams und Unternehmen ermöglicht, das Framework in einer gemeinsamen Umgebung zu erleben.
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
    accordion_aos_animation: fade-left
    accordion_aos_duration: 1600
    aaccordion_os_offset: 200
  video_spotlight:
    title: |
      Tauchen Sie ein in TeamOps
    subtitle: Die Welt hat viele Meinungen über die Zukunft der Arbeit.
    description:
      "Der TeamOps-Kurs hilft Organisationen dabei, größere Fortschritte zu erzielen, indem er die Beziehung Ihrer Teammitglieder als Problem behandelt, das operationalisiert werden kann.\n\n\n
       In nur wenigen Stunden können Sie tief in alle Leitprinzipien und Handlungsgrundsätze des Modells eintauchen und beginnen, die Kompatibilität mit den aktuellen Betriebspraktiken Ihres Teams zu bewerten."
    video:
      url: 754916142?h=56dd8a7d5d
      alt: team ops hero image
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Melden Sie sich jetzt an
      data_ga_name: enroll your team
      data_ga_location: body
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  card_section:
    title: |
      Leitprinzipien von TeamOps
    subtitle: Organisationen brauchen Menschen und Teams – ihre Kreativität, Perspektiven und Menschlichkeit.
    cards:
      - title: Geteilte Realität
        description: |
          Während andere Managementphilosophien die Geschwindigkeit des Wissenstransfers priorisieren, optimiert TeamOps die Geschwindigkeit des Wissensabrufs.
        icon:
          name: d-and-i
          slp_color: surface-700
        link:
          text: Erfahren Sie mehr
          url: /handbook/teamops/shared-reality/
        color: '#FCA326'
      - title: Jeder leistet seinen Beitrag
        description: |
          Unternehmen müssen ein System schaffen, in dem jeder unabhängig von Ebene, Funktion oder Standort Informationen nutzen und Beiträge leisten kann.
        icon:
          name: user-collaboration
          slp_color: surface-700
        link:
          text: Erfahren Sie mehr
          url: /handbook/teamops/everyone-contributes/
        color: '#966DD9'
      - title: Entscheidungsgeschwindigkeit
        description: |
          Der Erfolg korreliert mit der Entscheidungsgeschwindigkeit: die Menge der Entscheidungen, die in einem bestimmten Zeitraum getroffen werden, und die Ergebnisse, die sich aus einem schnelleren Fortschritt ergeben.
        icon:
          name: speed-alt-2
          slp_color: surface-700
        link:
          text: Erfahren Sie mehr
          url: /handbook/teamops/decision-velocity/
        color: '#FD8249'
      - title: Measurement clarity
        description: |
          Hier geht es darum, die richtigen Dinge zu messen. Die Entscheidungsprinzipien von TeamOps sind nur nützlich, wenn Sie Ergebnisse ausführen und messen.
        icon:
          name: target
          slp_color: surface-700
        link:
          text: Erfahren Sie mehr
          url: /handbook/teamops/measurement-clarity/
        color: '#256AD1'
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  join_us:
    title: |
      Der Bewegung beitreten
    description:
      "TeamOps ist ein organisatorisches Betriebsmodell, das Teams hilft, Produktivität, Flexibilität und Autonomie zu maximieren, indem Entscheidungen, Informationen und Aufgaben effizienter verwaltet werden. \n\n\n
       Schließen Sie sich einer wachsenden Liste von Organisationen an, die TeamOps praktizieren."
    list:
      title: Häufige Schmerzpunkte
      items:
        - Ad-hoc-Workflows verhindern die Ausrichtung
        - DIY-Management führt zu Funktionsstörungen
        - Die Kommunikationsinfrastruktur ist ein nachträglicher Gedanke
        - Besessenheit von Konsens verhindert Innovation
      icon: warning
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Melden Sie sich jetzt an
      data_ga_name: make your team better
      data_ga_location: body
    quotes:
      - text: Tests vor der Bereitstellung haben mehr Vertrauen geschaffen, dass das Produkt zur Veröffentlichung bereit ist; auch die Lieferfrequenz hat zugenommen.
        author: John Lastname
        note: Direktor der Berufsbezeichnung, Firmenname
      - text: Tests vor der Bereitstellung haben mehr Vertrauen geschaffen, dass das Produkt zur Veröffentlichung bereit ist; auch die Lieferfrequenz hat zugenommen.
        author: John Lastname
        note: Direktor der Berufsbezeichnung, Firmenname
    clients:
      - logo: /nuxt-images/home/logo_cncf_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_goldman_sachs_mono.svg
        alt: Goldman Sachs logo
      - logo: /nuxt-images/home/logo_siemens_mono.svg
        alt: Siemens logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/team-ops/logo_knowbe4_mono.svg
        alt: KnowBe4 logo
      - logo: /nuxt-images/home/logo_cncf_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_goldman_sachs_mono.svg
        alt: Goldman Sachs logo
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
