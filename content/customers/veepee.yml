---
  data:
    title: Veepee
    description: 'How Veepee accelerated deployment from 4 days to 4 minutes'
    og_title: Veepee
    twitter_description: 'How Veepee accelerated deployment from 4 days to 4 minutes'
    og_description: 'How Veepee accelerated deployment from 4 days to 4 minutes'
    og_image: /nuxt-images/blogimages/veepee.jpg
    twitter_image: /nuxt-images/blogimages/veepee.jpg
    customer: Veepee
    customer_logo: /nuxt-images/logos/logo_veepee.svg
    heading: 'How Veepee accelerated deployment from 4 days to 4 minutes'
    key_benefits:
      - label: Native CI integration
        icon: continuous-integration
      - label: Improved workflow
        icon: agile
      - label: Accelerated deployment
        icon: accelerate
    header_image: /nuxt-images/blogimages/veepee.jpg
    customer_industry: Retail
    customer_employee_count: 6,500
    customer_location: La Plaine Saint-Denis, France
    customer_solution: |
      [GitLab Enterprise](https://about.gitlab.com/pricing/)
    sticky_benefits:
      - label: Customer availability rate
        stat: 99.98%
      - label: Months adoption time
        stat: <3
      - label: Minute deployment, down from 4 days
        stat: 4
    blurb: Veepee completely overhauled its central repository and increased deployment and security with GitLab.
    introduction: |
        GitLab Enterprise helped transform Veepee’s IT production with enhanced continuous integration (CI), automated testing, and expedited continuous delivery (CD) for improved process workflows.
    quotes:
      - text: |
          Veepee’s availability rates went to 99.98% due to the faster feedback cycle and accelerated triggers.
        author: Antoine Millet
        author_role: Head of IT Operations
        author_company: Veepee
    content:
      - title: European e-commerce company
        description: |
          Veepee, formerly known as vente-privee.com, was founded in 2001 as an e-commerce company that specializes in flash online sales. Veepee creates daily sales events in partnership with leading brands, on a limited-time basis with specific products, sold at heavily discounted prices for industrial or promotional reasons.

          Based throughout Europe, <a href="http://www.veepee.com/" target="_blank">Veepee</a> represents the successful transformation of a long-established activity — clearance sales — through a revolutionary e-commerce model that has since been copied worldwide. vpTech, the Veepee Tech community, supports the business and the growth. The team handles more than 70 products using around 50 technos. As a European company spread in six different countries, they advocate feature teams that are flexible, autonomous, and multicultural.
      - title: Spreading the inner-source philosophy to improve overall delivery process
        description: |
          The year 2016 was a turning point for Veepee that included expanding the company throughout Europe. To get ahead of any upcoming issues, the vpTech team came with a strong promise: Align all Veepee legacies and adopt a modern and agile information system. To do so, the team grew up from 300 to 750 people.

          At the beginning, vpTech faced several challenges with its initial release workflow. Each team worked within their own toolset, which made collaboration and contribution difficult. Teams were frustrated that they couldn’t easily access other’s code and had no way to successfully collaborate with others.

          Also, with the previous workflow, deploying to production was a slow and tedious process. “It was not an ideal situation at all, as it was nearly impossible to release something fast — only emergency cases were free from the whole process,” explained Antoine Millet, Head of IT Operations at Veepee.

          vpTech was looking for a way to modernize its engineering infrastructure and enforce inner-source. At the same time, top management wanted to keep code and instances in house and stay off the cloud for the time being. CI integration was  a key requirement for the development teams.
      - title: GitLab and its complete toolsets
        description: |
          vpTech chose to fully reboot its tooling and workflow to modernize engineering tools within the company in order to allow teams to work in a singular environment with common tool sets and shared methodologies. vpTech researched and ended up adopting GitLab. “Three years ago, the biggest difference between GitLab and GitHub was the native CI integration. That's something we really needed at Veepee,” Millet said.

          The team adopted GitLab rather quickly. The initial goal was to start with 300 licenses over the span of three years. However, developers gravitated toward the platform and demanded more within the first six months of the adoption period. Within a year, vpTech had over 1,000 licenses.

          vpTech set out with new technology and deployment initiatives. The first initiative was to get developers onboarded to GitLab and completely off of the previous workflow. The second was to explain and spread the inner-source philosophy: If you need something, contribute. The third was to ensure that if a deployment in production happens, testing occurs in the GitLab [CI pipeline](/topics/ci-cd/pipeline-as-code/) for visibility.
      - title: A new workflow, a changed culture
        description: |
          Engineering and product team workflow has improved significantly. If the team wants to change a form on the website, or introduce a new A/B test, they simply release code. If the test passes, they can deploy in production. “Before GitLab, it could take up to four days for a production deployment because we had to involve a lot of people. Right now, it's a few minutes without any human in the middle. It's fully automatic,” Millet said.

          The technology and the deployment process with [GitLab CI](/stages-devops-lifecycle/continuous-integration/) belong to the product team, so they are empowered with choices for how they deploy. “We produced the last component of the CI, the deploy part. We created tools for engineering which will automatically execute blue and green deployment for them to try everything through canary deployment. If it's validated, it’s deployed in production. That's the front workflow,” Millet added.

          The site reliability engineering (SRE) team produced a lot of CI templates, which created a center of knowledge for the teams. If there is a new developer in the company, templates can be easily accessed in order to get started. “We have sets of linter for all languages pre-templated by the SRE team. Basically, you just choose that first component. In the middle, for the test, you put your own code because you know what you did, so the test part is for your team,” according to Millet. The deployment is templated for either Nomad and Kubernetes to get in production.

          In the last three years since adopting GitLab, Millet has never received a single complaint about the platform. GitLab significantly changed the culture of a 16-year-old company by improving transparency, workflow, and developers’ time. “It took almost a year for people to adopt GitLab as it was a change of habit, change of the way to work. Now it's working really well. At this point, we have developed a tool named DevHub on top of GitLab to make statistics and give a full view for the top management,” Millet said.
